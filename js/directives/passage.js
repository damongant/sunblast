sunblast.directive("passage", function(storyData, $compile) {
  return {
    restrict: "E",
    link: function(scope, element, attributes) {
      function update(id, scope) {
        if (typeof id === 'number') {
          var tpl = storyData.passages[id].render();
        } else {
          console.dir(id);
          var tpl = "<h1>404 - Passage not found</h1><p>The passage you attempted to load does not exist. Please notify the author.</p><p>Reload the page to continue</p>";
        }
        element.html("");
        element.append($compile(tpl)(scope));
      }
      scope.$watch("id", function(newValue, oldValue, scope) {
        update(newValue, scope);
      })
      update(attributes.id, scope);
    }
  };
});
