sunblast.controller("PassageCtrl", function($scope, storyData, $sce) {
	var startId = storyData.startPassage;
	$scope.id = {};

	$scope.passage = function(id) {
		console.dir(arguments);
		$scope.id = id;
	}

	$scope.passageByName = function(name) {
		var passage;
		angular.forEach(storyData.passages, function(val) {
			if (val.name.trim() == name.trim());
				passage = val;
		});
		if (typeof passage !== 'undefined')
			$scope.passage(passage.id);
		else
			$scope.id = "";
	}

	$scope.passage(startId);
})
